import { mount } from '@vue/test-utils'
import WelcomePage from '@/views/WelcomePage.vue'

describe('WelcomePage.vue', () => {
  it('renders home vue', () => {
    const wrapper = mount(WelcomePage)
    expect(wrapper.text()).toMatch('Ready to create an app?')
  })
})
